# OpenML dataset: Ottawa-Real-Estate-Data

https://www.openml.org/d/43417

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Real Estate data from the Canadian capital city of Ottawa for you to test your data analytics skill. This is my first dataset in Kaggle. Please let me know of anything that needs to be changed.
Content
Data was collected from real estate listings in Ottawa, Canada with minor changes made to location for privacy. 
Inspiration
Your data will be in front of the world's largest data science community. What questions do you want to see answered?
Collection of this data set was inspired by Kaggle's introductory course on Melbourne housing market. Anyone who wants to try out the skills they learned from their will surely benefit this dataset.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43417) of an [OpenML dataset](https://www.openml.org/d/43417). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43417/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43417/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43417/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

